#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#include <QTimer>
#include <QFileSystemModel>
#include <QDir>
#include <QList>
#include <QTime>
#include <QFileDialog>
#include <QSettings>
#include <QPixmap>
#include <QTranslator>
#include <QLocale>
#include <QGraphicsPixmapItem>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QtMultimedia/QMediaPlayer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;

    QSerialPort *serial;
    QMediaPlayer *player;
    QTimer *timer;
    QFileSystemModel *model;
    QList<QUrl> *files;
    QSettings *settings;

    void openSerialPort(const QString &name);
    void closeSerialPort();
    void playRandomMedia();
    void reloadDirectoryMedia(const QString& newdir);
    void loadSettings();

private slots:
    void readData();
    void handleError(QSerialPort::SerialPortError error);
    void playbackStopped(QMediaPlayer::MediaStatus status);
    void on_pushButtonOpen_clicked();
    void on_actionAbout_Qt_triggered();
    void on_actionAbout_this_program_triggered();
    void on_toolButton_clicked();
    void on_actionExit_triggered();
    void on_spinBoxThreshold_valueChanged(int arg1);
    void on_pushButtonPlayManually_clicked();
};

#endif // MAINWINDOW_H
