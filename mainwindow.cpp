#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Speaking Tree Application") + " 1.2");
    this->setWindowIcon(QIcon(":images/tree.png"));

    timer = new QTimer(this);
    timer->setInterval(50); // milliseconds wait before read data from the port
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(readData()));

    serial = new QSerialPort(this);
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
    connect(serial, SIGNAL(readyRead()), timer, SLOT(start())); // start reading only after delay to ensure all data is received

    files = new QList<QUrl>;

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    player = new QMediaPlayer(this);
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(playbackStopped(QMediaPlayer::MediaStatus)));

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->comboBoxPorts->addItem(info.portName() + " - " + info.description());
    }

    settings = new QSettings("AntLabs", "SpeekingTree");

    model = new QFileSystemModel(this);

    loadSettings();

    QPixmap pm(":images/stopped.png");
    ui->labelPlayback->setPixmap(pm);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openSerialPort(const QString& name)
{
    serial->setPortName(name);
    if(serial->open(QIODevice::ReadOnly))
    {
        if(
                serial->setBaudRate(QSerialPort::Baud9600) &&
                serial->setDataBits(QSerialPort::Data8) &&
                serial->setParity(QSerialPort::NoParity) &&
                serial->setStopBits(QSerialPort::OneStop) &&
                serial->setFlowControl(QSerialPort::NoFlowControl)
                )
        {
            ui->statusBar->showMessage(tr("Waiting for device..."));
            ui->pushButtonOpen->setText(tr("Close port"));
        }
        else
        {
            serial->close();
            QMessageBox::critical(this, tr("Error"), serial->errorString());
            ui->statusBar->showMessage(tr("Open error"));
        }
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), serial->errorString());
        ui->statusBar->showMessage(tr("Configure error"));
    }
}

void MainWindow::playRandomMedia()
{
    QMediaPlayer::MediaStatus status = player->mediaStatus();
    if(
            status == QMediaPlayer::EndOfMedia ||
            status == QMediaPlayer::LoadedMedia ||
            status == QMediaPlayer::NoMedia
        )
    {
        qDebug() << "About to play, total " << files->size();
        for(int i = 0; i < files->size(); i++)
        {
            qDebug() << files->at(i);
        }
        if(files->size() > 0)
        {
            int index = qrand() % files->size();
            QUrl next = files->at(index);
            qDebug() << "Playing " << next << " at " << index;
            player->setMedia(next);
            player->play();

            QPixmap pm(":images/playing.png");
            ui->labelPlayback->setPixmap(pm);
        }
    }
    else
    {
        //QMessageBox::critical(this, tr("Error"), tr("Cannot play media. Media status ") + QString("%1").arg(status));
    }
}

void MainWindow::closeSerialPort()
{
    serial->close();
    ui->statusBar->showMessage(tr("Port closed"));
}

void MainWindow::readData()
{
    QByteArray data = serial->readAll();
    data.chop(2); // cut \r\n
    ui->statusBar->showMessage(tr("Current level is ") + data);
    if(data.toInt() < ui->spinBoxThreshold->value())
    {
        qDebug() << "Threshold reached " << data << "media status " << player->mediaStatus();
        playRandomMedia();
    }
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if(error == QSerialPort::ResourceError)
    {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::playbackStopped(QMediaPlayer::MediaStatus status)
{
    QPixmap pm(":images/stopped.png");
    switch(status)
    {
    case QMediaPlayer::EndOfMedia:
        qDebug() << "Playback stopped";
        ui->labelPlayback->setPixmap(pm);
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButtonOpen_clicked()
{
    if(serial->isOpen())
    {
        serial->close();
        ui->pushButtonOpen->setText(tr("Open port"));
        ui->statusBar->showMessage(tr("Port closed"));
        return;
    }

    const QString portName = ui->comboBoxPorts->currentText().split(" - ")[0];
    qDebug() << portName;
    openSerialPort(portName);
    settings->setValue("port", ui->comboBoxPorts->currentText());
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_actionAbout_this_program_triggered()
{
    QMessageBox::about(this,tr("About this program"), tr("Speaking Tree by Oleg Antonyan for Speaking Tree social project. Sources are available on https://bitbucket.org/antlabs_dev/speaking-tree-program/"));
}

void MainWindow::on_toolButton_clicked()
{
    QString selected = QFileDialog::getExistingDirectory(this, tr("Select folder with media files"), ".", QFileDialog::ShowDirsOnly);
    if(selected.length() != 0)
    {
        qDebug() << selected;
        ui->listViewMedia->setRootIndex(model->index(QDir::toNativeSeparators(selected)));
        reloadDirectoryMedia(selected);
    }
}

void MainWindow::reloadDirectoryMedia(const QString& newdir)
{
    if(!files)
    {
        return;
    }
    files->clear();
    QDir dir(QDir::toNativeSeparators(newdir));
    QFileInfoList list = dir.entryInfoList(QDir::Files);
    foreach(QFileInfo finfo, list)
    {
        if(finfo.isFile() && finfo.suffix() == "wav")
        {
            files->append(QUrl::fromLocalFile(finfo.absoluteFilePath()));
            qDebug() << finfo.absoluteFilePath();
        }
    }
    settings->setValue("dir", newdir);
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::loadSettings()
{
    QString dir = settings->value("dir").toString();
    int threshold = settings->value("threshold").toInt();
    QString port = settings->value("port").toString();

    model->setRootPath(dir);
    ui->listViewMedia->setModel(model);
    ui->listViewMedia->setRootIndex(model->index(dir));
    reloadDirectoryMedia(dir);

    ui->spinBoxThreshold->setValue(threshold);

    ui->comboBoxPorts->setCurrentText(port);
}


void MainWindow::on_spinBoxThreshold_valueChanged(int arg1)
{
    settings->setValue("threshold", arg1);
}

void MainWindow::on_pushButtonPlayManually_clicked()
{
    playRandomMedia();
}
