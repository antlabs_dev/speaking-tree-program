#-------------------------------------------------
#
# Project created by QtCreator 2013-07-24T22:34:29
#
#-------------------------------------------------

QT  += core gui serialport multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpeakingTree
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    exit.png \
    about.png \
    tree.png \
    qt.png \
    playing.png \
    stopped.png \
    speakinktree_ru.ts \
    trans_ru.ts \
    trans_ru.qm

RESOURCES += \
    resources.qrc

TRANSLATIONS  += trans_ru.ts
