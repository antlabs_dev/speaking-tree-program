<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Главное окно</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="mainwindow.cpp" line="151"/>
        <source>Open port</source>
        <translation>Открыть порт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <source>Threshold</source>
        <translation>Порог срабатывания</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Play manually</source>
        <translation>Воспроизвести вручную</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>Playback</source>
        <translation>Воспроизведение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="119"/>
        <source>Media location</source>
        <translation>Звуковые файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="162"/>
        <source>Select media location</source>
        <translation>Выберите папку со звуковыми файлами</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="171"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="174"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="183"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="192"/>
        <location filename="mainwindow.cpp" line="169"/>
        <source>About this program</source>
        <translation>Об этой программе</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="7"/>
        <source>Speaking Tree Application</source>
        <translation>Говорящее дерево</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="60"/>
        <source>Waiting for device...</source>
        <translation>Ожидание ответа устройства...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="61"/>
        <source>Close port</source>
        <translation>Закрыть порт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="66"/>
        <location filename="mainwindow.cpp" line="72"/>
        <location filename="mainwindow.cpp" line="101"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Open error</source>
        <translation>Ошибка открытия порта</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Configure error</source>
        <translation>Ошибка конфигурации порта</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="101"/>
        <source>Cannot play media. Media status </source>
        <translation>Невозможно воспроизвести файл. Статус воспроизведения </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="152"/>
        <source>Port closed</source>
        <translation>Порт закрыт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <source>Current level is </source>
        <translation>Текущий уровень </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Critical Error</source>
        <translation>Фатальная ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="169"/>
        <source>Speaking Tree by Oleg Antonyan for Speaking Tree social project. Sources are available on https://bitbucket.org/antlabs_dev/speaking-tree-program/</source>
        <translation>Говорящее дерево. Автор Олег Антонян, для социального проекта Моё Знакомое Дерево. Исходный код доступен на https://bitbucket.org/antlabs_dev/speaking-tree-program/ </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="174"/>
        <source>Select folder with media files</source>
        <translation>Выберите папку где расположены звуковые файлы</translation>
    </message>
</context>
</TS>
