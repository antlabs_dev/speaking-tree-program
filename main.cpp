#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString locale = QLocale::system().name();
    QTranslator trans;
    trans.load(":translations/trans_" + locale);
    a.installTranslator(&trans);

    MainWindow w;
    w.show();
    
    return a.exec();
}
